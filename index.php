<?php
session_start ();

// Front Controller
// We run the Model, the Controller and the Configuration variables

require_once 'app/model.php';
require_once 'app/controller.php';
require_once 'app/config.php';

// we create the rutes

$map = array (
		
		'home' => array (
				'controller' => 'Controller',
				'action' => 'home' 
		),
		'addContact' => array (
				'controller' => 'Controller',
				'action' => 'addContact' 
		),
		'viewContact' => array (
				'controller' => 'Controller',
				'action' => 'viewContact' 
		),
		'listAllContacts' => array (
				'controller' => 'Controller',
				'action' => 'listAllContacts' 
		),
		'deleteContact' => array (
				'controller' => 'Controller',
				'action' => 'deleteContact' 
		) 
);

// We parse the route

if (isset ( $_GET ['ctl'] )) {
	if (isset ( $map [$_GET ['ctl']] )) {
		$rute = $_GET ['ctl'];
	} else {
		header ( 'Status: 404 Not Found' );
		echo '<html><body><h1>Error 404: The route does not exist <i> </body></html>';
		exit ();
	}
} else {
	$rute = 'home';
}

$controller = $map [$rute];

// We run the controller asociated with the selected rute

if (method_exists ( $controller ['controller'], $controller ['action'] )) {
	call_user_func ( array (
			new $controller ['controller'] (),
			$controller ['action'] 
	) );
} else {
	header ( 'Status: 404 Not Found' );
	echo '<html><body><h1>Error 404: The route does not exist <i> </body></html>';
}

?>
