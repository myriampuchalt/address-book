<?php

ob_start ();

if (isset ( $result ['contacts'] )) {
	$user = $result ['contacts'];
	?>
<table class="table">
	<tr>
		<th>#</th>
		<th>Name</th>
		<th>Company</th>
		<th>Address</th>
		<th>Phone Number</th>
		<th>Email</th>
		<th>Notes</th>
		<th></th>
	</tr>
	<tr>
		<td><?php echo $user['id']?></td>
		<td><?php echo $user['name']?></td>
		<td><?php echo $user['company']?></td>
		<td><?php echo $user['address']?></td>
		<td><?php echo $user['phone_number']?></td>
		<td><?php echo $user['email']?></td>
		<td><?php echo $user['notes']?></td>
	</tr>
<?php }?>

</table>
<?php

$content = ob_get_clean ();
require dirname(__FILE__).'/layout.php';
 
 ?>