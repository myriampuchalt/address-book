<?php ob_start ();
if (isset ( $result ['contacts'] )) {
	$users = $result ['contacts'];
?>
<table class="table">
	<tr>
		<th>Name</th>
		<th>Phone Number</th>
		<th></th>
		<th></th>
	</tr>
<?php foreach($users as $element): ?>
<tr>
		<td><?php echo $element['name']?></td>
		<td><?php echo $element['phone_number']?></td>
		<td><form id='contacts' name="contacts" method="POST"
				action="index.php?ctl=viewContact&id=<?php echo $element['id']?>">
				<button type="submit" name="viewUser">View Details</button>
			</form></td>
		<td><form id='contacts' name="contacts" method="POST"
				action="index.php?ctl=deleteContact&id=<?php echo $element['id']?>">
				<button type="submit" name="deleteUser">Delete</button>
			</form></td>
	</tr>
<?php endforeach; }?>

</table>
<?php
$content = ob_get_clean ();
require dirname ( __FILE__ ) . '/layout.php';
?>