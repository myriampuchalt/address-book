<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>AddressBook WorldStores</title>
<meta name="description"
	content="Application that adds, views and deletes contacts to / from an address book">
<meta name="author" content="Myriam Puchalt Gisep">
<meta name="viewport" content="width=device-width; initial-scale=1.0">

<link type="text/css" rel="stylesheet" href="<?php echo Config::$css ?>" />

<script src="<?php echo Config::$jquery ?>"></script>
<script type="text/javascript" src="<?php echo Config::$js ?>"> </script>

</head>
<body>
	<div id="mypage">

		<!--  Default Navbar with Bootsrap -->
		<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand" href="index.php?ctl=home">Address Book</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php?ctl=addContact">Add</a></li>
					<li><a href="index.php?ctl=listAllContacts">Show Contacts</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid --> </nav>

		<div id="content">
            
        	<?php if(isset($content)) echo $content; else  ?>
            
    	</div>

	</div>

</body>
</html>
