<?php ob_start (); ?>

<section>
	<h3>Contact Details:</h3>

	<form id='contactInformation' role="form" method="POST"
		action="index.php?ctl=addContact">
		<div class="form-group">
			<label for="name" class="control-label"></label> <input type="text"
				class="form-control" name="name" placeholder="Name" required>
		</div>
		<div class="form-group">
			<label for="company" class="control-label"></label> <input
				type="text" class="form-control" name="company"
				placeholder="Company">
		</div>
		<div class="form-group">
			<label for="address" class="control-label"></label> <input
				type="text" class="form-control" name="address"
				placeholder="Address">
		</div>
		<div class="form-group">
			<label for="phoneNumber" class="control-label"></label> <input
				type="text" data-minlength="10" class="form-control"
				name="phoneNumber" placeholder="Phone Number" required>
		</div>
		<div class="form-group">
			<label for="email" class="control-label"></label> <input type="email"
				class="form-control" name="email" placeholder="Email"
				data-error="The email address is invalid">
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group">
			<label for="notes" class="control-label"></label> <input type="text"
				class="form-control" name="notes" placeholder="Notes">
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary" name="addContact">Save
				Details</button>
		</div>
	</form>
</section>

<?php
$content = ob_get_clean ();
require dirname ( __FILE__ ) . '/layout.php';
?>
