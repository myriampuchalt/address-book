<?php

class Controller {
    
	public $model;
	
	// Constructor
	public function Controller() {
		$this->model = new Model ( Config::$dbe, Config::$user, Config::$password, Config::$host );
	}
	
	// Home
	public function home() {
		require dirname ( __FILE__ ) . '/views/home.php';
	}
	
	// Insert into DB
	public function addContact() {
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$this->model->createContact ( $_POST ['name'], $_POST ['company'], $_POST ['address'], $_POST ['phoneNumber'], $_POST ['email'], $_POST ['notes'] );
			header ( 'Location: index.php?ctl=listAllContacts' );
		}
		require dirname ( __FILE__ ) . '/views/addContact.php';
	}
	
	// Get from DB
	public function viewContact() {
		
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$result = array();
			$result ['contacts'] = $this->model->getContact ( $_GET ['id'] );
		}
		require dirname ( __FILE__ ) . '/views/viewContact.php';
	}
	
	public function listAllContacts() {
		$result = array();
		$result ['contacts'] = $this->model->getContacts ();
		require dirname ( __FILE__ ) . '/views/listContacts.php';
	}
	
	// Delete from DB
	public function deleteContact() {
		if ($_SERVER ['REQUEST_METHOD'] == 'POST') {
			$this->model->deleteContact ( $_GET ['id'] );
			header ( 'Location: index.php?ctl=listAllContacts' );
		}
		require dirname ( __FILE__ ) . '/views/listContacts.php';
	}
}
?>