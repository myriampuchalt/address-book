<?php
class Model {
    
	protected $connection;

    // Constructor
	public function Model($db, $user, $password, $host) {
	    
        try {
          	$connection = new PDO("mysql:host=$host;dbname=$db;charset=utf8",$user,$password);  
        } catch (PDOException $err) {  
            echo "The connection to mysql server failed";
            $err->getMessage() . "<br/>";
            file_put_contents('PDOErrors.txt',$err, FILE_APPEND);  // write some details to an error-log outside public_html  
            die();  //  terminate connection
        }
    }
	
	// Insert into DB
	public function createContact($name, $company, $address, $phoneNumber, $email, $notes) {
	    
	    $sql = "INSERT INTO contacts (name, company, address, phone_number, email, notes) values('$name', '$company', '$address', '$phoneNumber', '$email', '$notes')";
	    $stmt = $connection->prepare($sql);
		$stmt->execute();

	}
	
	// Get from DB
	public function getContact($id) {

		$sql = "SELECT * FROM contacts WHERE id= :id";
		$stmt = $connection->prepare($sql);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		
		while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
		    $user [] = $result;
		}
		return $user [0];
		
		$connection = NULL;
		
	}
	
	public function getContacts() {
	    
	    $sql = "SELECT * FROM contacts ORDER BY id ASC";
		$stmt = $connection->prepare($sql);
		$stmt->execute();
		
		while ($result = $stmt->fetch(PDO::FETCH_ASSOC)){
		    $users [] = $result;
		}
    
        return $users;
		
		$connection = NULL;
		
	}
	
	// Delete from DB
	public function deleteContact($id) {
	    
		$sql = "DELETE FROM contacts WHERE id= :id";
		$stmt = $connection->prepare($sql);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		
		$connection = NULL;
		
	}
}
?>